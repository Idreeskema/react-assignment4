import React from "react";

function Dropdown({ onSelected, selectname, options }) {
  const handlechange = (e) => {
    onSelected(e.target.value);
  };
  return (
    <form>
      <select className="ui dropdown" name={selectname} onChange={handlechange}>
        {options &&
          options.map((c) => (
            <option key={c} value={c}>
              {c}
            </option>
          ))}
      </select>
    </form>
  );
}

export default Dropdown;
